import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import ImaggeView from 'react-native-image-view';
// import Database from './Database';

// const db = new Database();rrr

export default function App() {
  // const [ siswa, getSiswa ] = useState([])
  // const blue = '#1e88e5'

  // useEffect(() => {
  //   fetchSiswa();
  // }, [])

  // const fetchSiswa = async () => {
  //   let data = await db.getSiswa();
  //   console.log(data);
  // }
  const siswa = [
    {
      nama: "John Wick",
      kelas: "XII TGB 1",
      motto: "Dengan Sebuah Pensil, Kita Bisa Merubah Dunia",
      img: require("./assets/img/imgjw.jpg")
    },
    {
      nama: "Jeff Bejos",
      kelas: "XII RPL 3",
      motto: "Uang Bukan Segalanya",
      img: require("./assets/img/imgjb.jpg")
    },
    {
      nama: "Keiichi Tsuchiya",
      kelas: "XII TKR 4",
      motto: "I drift not because it is a quicker way around a corner, but it is the most exciting way...",
      img: require("./assets/img/imgkt.jpg")
    },
    {
      nama: "Akio Toyoda",
      kelas: "XII TKR 9",
      motto: "Without Supra, I Couldn't Be a Master Driver",
      img: require("./assets/img/imgat.jpg")
    }
  ]
  return (
    <>
    <View style={style.card}>
      <Text style={{color: 'white', fontSize: 25, fontWeight: "bold" }}>Header</Text>  
    </View>    
    {siswa.map((a, i) => {
      return <DataCard key={i} nama={a.nama} kelas={a.kelas} motto={a.motto} img={a.img} />
    })}
    </>
  );
}
const DataCard = props => {
  // let a = props.img;
  // let imgPath = require('./assets/img/'+a);
  console.log(props.img)
  return (
  <View key={props.key} style={ style.box}>
      <View style={{flex: 1, height: 100, backgroundColor: "red", }}>
        <Image style={{width: "100%", height:"100%"}} source={props.img} />
      </View>
      <View style={{flex: 4, height: 100, backgroundColor: "yellow", flexDirection: 'column'}}>
        <View style={{flex: 2, height: '100%', flexDirection: 'row'}}>
          <View style={{flex: 1, height: '100%', backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center', padding: 10}}>
            <Text>{props.nama}</Text>
          </View>
          <View style={{flex: 1, height: '100%', backgroundColor: 'orange', justifyContent: 'center', alignItems: 'center', padding: 10}}>
            <Text>{props.kelas}</Text>
          </View>
        </View>
        <View style={{flex: 4, height: '100%', backgroundColor: "cyan", justifyContent: 'center', alignItems: 'center', padding: 10, textAlign: 'center'}}>
        <Text>"{props.motto}"</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
  },
});
const style = {
  card : {
    marginTop: 25, height: 60, backgroundColor: "#1e88e5",shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  center : {
    justifyContent: 'center', 
    alignItems: 'center'
  },
  box: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 4.65,  
    elevation: 8,
    flexDirection: 'row', height: 90, margin: 10, marginTop: 20
  }
}
